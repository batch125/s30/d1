const express = require(`express`);
const mongoose = require(`mongoose`);

const app = express();
const port = 3000;


app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect(`mongodb+srv://zuittDBBatch125:70pez0265@cluster0.l6bhm.mongodb.net/b125-tasks?retryWrites=true&w=majority`,
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}).then(()=> {
		console.log(`Successfully connected to the Database`);
	}).catch((error)=> {
		console.log(error);
	});

	//schema - gives a structure of the kind of record/document that are going to be contained in the database

	//Schema() method - determins the structure of documents to be written in the database. Schema acts as a blueprint to our data

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: Boolean,
		default: false
	}
})
const Task = mongoose.model(`Task`,taskSchema);

app.post(`/add-task`,(req,res)=>{
	let newTask = new Task({
		name: req.body.name	
	});
	newTask.save((error, savedTask)=>{
			if(error){
				console.log(error);
			}
			else {
				res.send(`New task is saved. ${savedTask}`);
			}
	});

});

//mini activity #1
const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	userName: String,
	password: String
	
})
const User = mongoose.model(`User`,userSchema);

//mini activity #2
app.post(`/register`,(req,res)=>{
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,	
		userName: req.body.userName,
		password: req.body.password,
	});
	newUser.save((error, savedUser)=>{
			if(error){
				console.log(error);
			}
			else {
				res.send(`New user is saved. ${savedUser}`);
			}
	});

});

//Retrieve

app.get('/retrieve-tasks', (req, res)=> {
	Task.find({}, (error, records)=> {
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	});
});

app.get('/retrieve-tasks-done', (req, res)=> {
	Task.find({status:true}, (error, records)=> {
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	});
});

app.put('/complete-task/:taskId', (req, res)=> {
	let taskId = req.params.taskId;
	Task.findByIdAndUpdate(taskId, { status:true }, (error, updatedTask)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(`Task completed successfully`);
		}
	});
});

app.delete('/delete-task/:taskId', (req, res)=> {
	let taskId = req.params.taskId;
	Task.findByIdAndDelete(taskId,(error, deletedTask)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(`Task deleted`);
		}
	});
});

app.listen(port,()=> console.log(`Server is running at ${port}`));
